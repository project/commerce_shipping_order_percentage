Charge a shipping rate on a percentage of the total order value, provides min & max values.

Post-Installation
Once installed navigate to /admin/commerce/shipping-methods and add a shipping method, selecting 'Percentage of Order value' and filling in the form.