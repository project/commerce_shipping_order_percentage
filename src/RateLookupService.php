<?php

namespace Drupal\commerce_shipping_order_percentage;

use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_shipping\Entity\ShipmentInterface;

/**
 * Class RateLookupService.
 */
class RateLookupService implements RateLookupServiceInterface {

  /**
   * Entity\Order definition.
   *
   * @var \Drupal\commerce_order\Entity\Order
   */
  protected Order $order;

  /**
   * Shipment.
   *
   * @var \Drupal\commerce_shipping\Entity\ShipmentInterface
   */
  protected ShipmentInterface $commerceShipment;

  /**
   * Set the shipment for rate requests.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $commerce_shipment
   *   A Drupal Commerce shipment entity.
   */
  public function setShipment(ShipmentInterface $commerce_shipment): void {
    $this->commerceShipment = $commerce_shipment;
  }

  /**
   * Return rates for shipment method.
   *
   * @inheritdoc
   */
  public function getRates(ShipmentInterface $shipment, array $config):array {
    // All rates available are ready to return from this call.


    if ($shipment->getShippingProfile()->get('address')->isEmpty()) {
      return [];
    }
    $shipping_total = [];

    // Charge lots by default.
    $percentage_charge = $config['shipping_settings']['percentage'] ?? 100;

    $min_charge = $config['shipping_settings']['minimum_charge'] ?? 0;
    $max_charge = $config['shipping_settings']['maximum_charge'] ?? NULL;

    // At this point we should be charging a % of the total order value.
    // Let's get the order value and go from there.
    $shipping_total = ($shipment->getOrder()->getSubtotalPrice()->getNumber() * $percentage_charge) / 100;

    if ($shipping_total < $min_charge) {
      $shipping_total = $min_charge;
    }
    elseif (
      !empty($max_charge)
      && $max_charge !== 0
      && $shipping_total > $max_charge
    ) {
      $shipping_total = $max_charge;
    }

    return [$shipping_total];
  }

}
