<?php

namespace Drupal\commerce_shipping_order_percentage;

use Drupal\commerce_shipping\Entity\ShipmentInterface;

/**
 * Interface RateLookupServiceInterface.
 */
interface RateLookupServiceInterface {

  /**
   * Returns rates for order.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipping_profile
   *   Shipping profile.
   * @param array $config
   *   Shipping rate config.
   *
   * @return array
   *   Array of available rates.
   */
  public function getRates(ShipmentInterface $shipping_profile, array $config):array;

}
