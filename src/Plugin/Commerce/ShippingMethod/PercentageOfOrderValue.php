<?php

namespace Drupal\commerce_shipping_order_percentage\Plugin\Commerce\ShippingMethod;

use Drupal\commerce_price\Price;
use Drupal\commerce_price\RounderInterface;
use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Drupal\commerce_shipping\PackageTypeManagerInterface;
use Drupal\commerce_shipping\Plugin\Commerce\ShippingMethod\ShippingMethodBase;
use Drupal\commerce_shipping\ShippingRate;
use Drupal\commerce_shipping\ShippingService;
use Drupal\commerce_shipping_order_percentage\rateLookupService;
use Drupal\commerce_store\CurrentStoreInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\state_machine\WorkflowManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Provides the PercentageOfOrderValue shipping method.
 *
 * @CommerceShippingMethod(
 *   id = "percentage_of_order_value",
 *   label = @Translation("Percentage of Order value"),
 * )
 */
class PercentageOfOrderValue extends ShippingMethodBase {

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected EventDispatcherInterface $eventDispatcher;

  /**
   * The Service Plugins.
   *
   * @var \Drupal\Core\Plugin\DefaultLazyPluginCollection
   */
  protected $plugins;

  /**
   * Commerce Logger Channel.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected LoggerInterface $watchdog;

  /**
   * The Rate returner.
   *
   * @var \Drupal\commerce_shipping_order_percentage\RateLookupService
   */
  protected RateLookupService $percentageOfOrderLookupService;

  /**
   * Rate Lookups.
   *
   * @var \Drupal\commerce_shipping_order_percentage\rateLookupService
   */
  protected $rateLookupService;

  /**
   * The CurrentStoreInterface provides access to the current store context within which
   * the order is being processed. This is crucial for determining settings that may vary
   * per store, such as currency, which impacts how shipping costs are calculated and displayed.
   *
   * @var \Drupal\commerce_store\CurrentStoreInterface
   */
  protected CurrentStoreInterface $currentStore;

  /**
   * The RounderInterface is used to ensure that all prices, including shipping rates,
   * are rounded according to the specific rounding rules defined in the commerce configuration.
   * This helps in maintaining pricing consistency across different parts of the system.
   *
   * @var \Drupal\commerce_price\RounderInterface
   */
  protected RounderInterface $rounder;

  /**
   * @var mixed
   */
  protected $services;

  /**
   * Constructs a new PercentageOfOrderValue object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\commerce_shipping\PackageTypeManagerInterface $package_type_manager
   *   The package type manager.
   * @param \Drupal\state_machine\WorkflowManagerInterface $workflow_manager
   *   The workflow manager.
   * @param RateLookupService $rate_lookup_service
   *   Rate lookup service.
   * @param \Drupal\commerce_store\CurrentStoreInterface $currentStore
   *   The current store interface, providing context about the store.
   * @param \Drupal\commerce_price\RounderInterface $rounder
   *   Service for rounding prices according to commerce configurations.
   */
  public function __construct(
    array $configuration,
    string $plugin_id,
    $plugin_definition,
    PackageTypeManagerInterface $package_type_manager,
    WorkflowManagerInterface $workflow_manager,
    RateLookupService $rate_lookup_service,
    CurrentStoreInterface $currentStore,
    RounderInterface $rounder
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $package_type_manager, $workflow_manager);
    $this->rateLookupService = $rate_lookup_service;
    $this->currentStore = $currentStore;
    $this->rounder = $rounder;
    $rate_label = $this->configuration['rate_label'] ?? $this->t('Default Shipping Service');
    $this->services['default'] = new ShippingService('default', $rate_label);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.commerce_package_type'),
      $container->get('plugin.manager.workflow'),
      $container->get('commerce_shipping_order_percentage.ratelookup'),
      $container->get('commerce_store.current_store'),
      $container->get('commerce_price.rounder')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
        'rate_label' => $this->t('Direct to your door'),
        'rate_description' => '',
        'shipping_settings' => [
          'percentage' => 10,
          'minimum_charge' => 0,
          'maximum_charge' => 150,
        ],
        'services' => ['default'],
      ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['rate_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Rate label'),
      '#description' => $this->t('Shown to customers when selecting the rate.'),
      '#default_value' => $this->configuration['rate_label'],
      '#required' => TRUE,
    ];

    $form['shipping_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Rate details'),
      '#description' => $this->isConfigured() ? $this->t('Update your rates.') : $this->t('Fill in your shipping rates.'),
      '#weight' => $this->isConfigured() ? 10 : -10,
      '#open' => !$this->isConfigured(),
    ];

    $form['shipping_settings']['percentage'] = [
      '#type' => 'number',
      '#title' => $this->t('Percentage'),
      '#description' => $this->t('Enter the percentage to charge on the order value, eg 10, 17.5 etc No % sign.'),
      '#default_value' => $this->configuration['shipping_settings']['percentage'] ?? 10,
      '#required' => TRUE,
    ];

    $form['shipping_settings']['minimum_charge'] = [
      '#type' => 'number',
      '#title' => $this->t('Minimum charge'),
      '#description' => $this->t('Enter the minimum amount to charge'),
      '#default_value' => $this->configuration['shipping_settings']['minimum_charge'] ?? 0,
    ];

    $form['shipping_settings']['maximum_charge'] = [
      '#type' => 'number',
      '#title' => $this->t('Maximum charge'),
      '#description' => $this->t('Enter the maximum amount to charge, leave empty or enter 0 if you do not want to use the maximum charge limit'),
      '#default_value' => $this->configuration['shipping_settings']['maximum_charge'] ?? 150,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state): void {
    // Min must be less than max if they are set.
    $values = $form_state->getValue($form['#parents']);

    if (isset($values['shipping_settings']['minimum_charge']) &&
      $values['shipping_settings']['maximum_charge'] !== 0
      && !empty($values['shipping_settings']['maximum_charge'])
      && $values['shipping_settings']['minimum_charge'] > $values['shipping_settings']['maximum_charge']
    ) {
      $form_state->setErrorByName('minimum_charge', $this->t('Minimum charge must be lower than Maximum charge, please check and resubmit.'));
    }

    parent::validateConfigurationForm($form, $form_state);

  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    if (!$form_state->getErrors()) {

      $values = $form_state->getValue($form['#parents']);
      $this->configuration['rate_label'] = $values['rate_label'];
      $this->configuration['shipping_settings']['percentage'] = $values['shipping_settings']['percentage'];
      $this->configuration['shipping_settings']['minimum_charge'] = $values['shipping_settings']['minimum_charge'];
      $this->configuration['shipping_settings']['maximum_charge'] = $values['shipping_settings']['maximum_charge'];
    }
    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function calculateRates(ShipmentInterface $shipment): array {
    $rates = [];
    if ($shipment->getShippingProfile()->address->isEmpty()) {
      return [];
    }

    if (!isset($this->services['default'])) {
      // Handle the case where the default service is not set.
      $this->services['default'] = new ShippingService('default', $this->t('Default Shipping Service'));
    }

    $availableRates = $this->rateLookupService->getRates($shipment, $this->configuration);
    $currency_code = $this->currentStore->getStore()->getDefaultCurrencyCode();

    foreach ($availableRates as $key => $rate) {
      $raw_price = new Price((string) $rate, $currency_code);
      $price = $this->rounder->round($raw_price);

      $rates[] = new ShippingRate([
        'shipping_method_id' => $this->parentEntity->id(),
        'service' => $this->services['default'],
        'amount' => $price,
      ]);
    }

    return $rates;
  }

  /**
   * Determine if we have the minimum information to return rates.
   *
   * We only really need a percentage, everything else is optional.
   *
   * @return bool
   *   TRUE if there is enough information to connect, FALSE otherwise.
   */
  protected function isConfigured():bool {

    if (isset($this->configuration['shipping_settings']) && !empty($this->configuration['shipping_settings']['percentage'])) {
      return TRUE;
    }
    return FALSE;
  }

}
